# frozen_string_literal: true

source 'https://rubygems.org'
ruby '2.4.1'

# Rails
gem 'rails', '~> 5.1.2'
gem 'activeadmin', github: 'activeadmin'

# ActiveRecord/Models
gem 'pg', '~> 0.18'
gem 'phony_rails', '~> 0.14.6'
gem 'strip_attributes', '~> 1.8.0'
gem 'draper', '~> 3.0.0'

# Orchestration
gem 'puma', '~> 3.4.0'
gem 'foreman', '~> 0.80.1'
gem 'dotenv-rails', '~> 2.1'

# Asset processing
gem 'sassc-rails', '~> 1.2'
gem 'uglifier', '>= 1.3.0'
gem 'coffee-rails', '~> 4.2'

# Assets
gem 'jquery-rails'

group :development, :test do
  # Debugging
  gem 'byebug', platform: :mri

  # Testing
  gem 'factory_girl_rails'
  gem 'faker'

  # Code Quality
  gem 'rubocop', require: false
end

group :development do
  # Debugging
  gem 'web-console', '~> 3.3'
  gem 'listen', '~> 3.0.5'

  # Preloading
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'

  # Testing
  gem 'letter_opener'
  gem 'meta_request'
  gem 'overcommit', require: false
end

group :test do
  # Testing
  gem 'mocha'
  gem 'webmock'
  gem 'capybara'
  gem 'poltergeist'
end
