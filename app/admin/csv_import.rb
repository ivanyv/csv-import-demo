# frozen_string_literal: true

ActiveAdmin.register CsvImport do
  menu false

  actions :all, except: %i[index show edit update destroy]

  breadcrumb do
    [
      link_to('Admin', admin_root_path),
      'CSV Import'
    ]
  end

  permit_params :file

  form do |_f|
    semantic_errors

    inputs do
      input :file, as: :file, input_html: { accept: '.csv' }
    end

    actions do
      action :submit, label: 'Import'
      cancel_link admin_people_path
    end
  end

  controller do
    include ActionView::Helpers::TextHelper

    def create
      create! do |format|
        format.html do
          if resource.errors.empty?
            redirect_to %i[admin people],
                        notice: "CSV Imported: #{pluralize(resource.count, 'item', 'items')} (total: #{resource.total})"
          else
            render
          end
        end
      end
    end
  end
end
