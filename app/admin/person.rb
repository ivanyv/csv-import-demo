# frozen_string_literal: true

ActiveAdmin.register Person do
  filter :name
  filter :email
  filter :address
  filter :created_at

  decorate_with PersonDecorator

  action_item :import_csv, only: :index do
    link_to 'Import CSV', %i[new admin csv_import]
  end

  index do
    selectable_column
    column :name do |person|
      link_to person, [:admin, person]
    end
    column :email do |person|
      link_to person.email, "mailto:#{person.email}"
    end
    column :phone do |person|
      link_to person.formatted_phone, "tel:#{person.phone}" if person.phone.present?
    end
    column :website do |person|
      link_to person.website_short, person.website, target: '_blank' if person.website.present?
    end
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :name
      row :email do
        link_to person.email, "mailto:#{person.email}" if person.email.present?
      end
      row :phone do
        link_to person.formatted_phone, "tel:#{person.phone}" if person.phone.present?
      end
      row :website do
        link_to person.website_short, person.website, target: '_blank' if person.website.present?
      end
      row :created_at
      row :updated_at
    end
  end

  permit_params :name, :email, :phone, :website
end
