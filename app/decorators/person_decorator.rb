class PersonDecorator < ApplicationDecorator
  delegate_all

  def website_short
    website.sub(%r{^https?://(www\.)?}, '')
  end

  def formatted_phone
    phone.to_s.phony_formatted
  end
end
