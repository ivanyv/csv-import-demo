# frozen_string_literal: true

class CsvImport
  include ActiveModel::Model

  attr_accessor :file
  attr_reader :count, :total

  def save
    import_into_people
  rescue CSV::MalformedCSVError
    errors.add :file, :invalid
  end

  private

  ATTR_MAP = {
    'Name' => :name,
    'Email Address' => :email,
    'Telephone Number' => :phone,
    'Website' => :website
  }.freeze

  def import_into_people
    @count = 0
    @total = 0

    CSV.parse(file.read, headers: true) do |row|
      attributes = ATTR_MAP.each_with_object({}) do |(csv_col, model_col), attrs|
        attrs[model_col] = row[csv_col]
      end
      @count += 1 if Person.create(attributes).persisted?
      @total += 1
    end
  end
end
