# frozen_string_literal: true

class Person < ApplicationRecord
  strip_attributes collapse_spaces: true, replace_newlines: true
  phony_normalize :phone, default_country_code: 'US'

  validates :name, :email, presence: true
  validates :email, uniqueness: true, email: true

  def to_s
    name
  end

  def website=(value)
    value.to_s.match?(%r{^https?://}) ? super : super("http://#{value}")
  end
end
