# frozen_string_literal: true

class CreatePeople < ActiveRecord::Migration[5.1]
  def change
    create_table :people do |t|
      t.string :name
      t.string :email, limit: 128
      t.string :phone, limit: 15
      t.string :website, limit: 128

      t.timestamps
    end

    add_index :people, :name
    add_index :people, :email, unique: true
  end
end
