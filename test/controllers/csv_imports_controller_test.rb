# frozen_string_literal: true

require 'test_helper'

class CsvImportsControllerTest < ActionDispatch::IntegrationTest
  test 'can import valid csv' do
    file = Rack::Test::UploadedFile.new(Rails.root.join('test/fixtures/people.csv'), 'text/csv')
    # Should create only 7, 2 have no email/name and one has duplicate email
    assert_difference 'Person.count', 7 do
      post admin_csv_imports_path, params: { csv_import: { file: file } }
      assert_redirected_to admin_people_path
    end
  end
end
