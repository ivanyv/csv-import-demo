# frozen_string_literal: true

FactoryGirl.define do
  factory :person do
    name { Faker::Name.name }
    email { Faker::Internet.email }
    address { Faker::Address.full_address }
    phone { Faker::PhoneNumber.phone_number }
    website { Faker::Internet.url }
  end
end
